
-module(filewatch).

-export([start/1,
         start/2,
         stop/1]).

-type watch_pair() :: {file:name_all(), term()}.
-export_type([watch_pair/0]).

%% ==================================================================
%% Define
%% ==================================================================

-define(DRIVER_NAME, filewatch).
-define(COOLDOWN_MS, application:get_env(filewatch, cooldown_ms, 3000)).
-define(CALL_ARG, 1).

-type handle() :: pid().
-type watch_descriptor() :: integer().
-type directory() :: file:filename().

-record(state,
        {pid :: pid(),
         port :: port(),
         watch_map :: #{
           {watch_descriptor(), file:filename()} => any(),
            directory()                          => watch_descriptor()
         }
        }).

%% ==================================================================
%% Public
%% ==================================================================

%% ---------------------------------
-spec start([filewatch:watch_pair()]) -> {ok, handle()} | {error, _}.
%% ---------------------------------
start(Pairs) ->
    start(self(), Pairs).

%% ---------------------------------
-spec start(pid(), [filewatch:watch_pair()]) -> {ok, handle()} | {error, _}.
%% ---------------------------------
start(Dest, Pairs) ->
    Pid = spawn_link(fun () -> init(Dest, Pairs) end),
    {ok, Pid}.

%% ---------------------------------
-spec stop(handle()) -> ok | {error, _}.
%% ---------------------------------
stop(Handle) ->
    Handle ! terminate,
    ok.

%% ==================================================================
%% Internal
%% ==================================================================

init(Pid, Pairs) ->
    ok = load(),
    Arg = io_lib:format("~p ~p", [?DRIVER_NAME, ?COOLDOWN_MS]),
    Port = open_port({spawn_driver, Arg}, [in]),
    WatchMap = create_watch_map(Pid, Pairs, Port, maps:new()),
    watch(#state{pid = Pid, port = Port, watch_map = WatchMap}).

%% -------
%% @private
load() ->
    case erl_ddll:load_driver(priv_dir(), ?DRIVER_NAME) of
        ok -> ok;
        {error, already_loaded} -> ok;
        {error, Message} ->
            error_logger:error_msg("~p: error loading driver: ~p",
                                   [?DRIVER_NAME,
                                    erl_ddll:format_error(Message)])
    end.

%% @private
priv_dir() ->
    case code:priv_dir(?MODULE) of
        {error, _} ->
            EbinDir = filename:dirname(code:which(?MODULE)),
            AppPath = filename:dirname(EbinDir),
            filename:join(AppPath, "priv");
        Dir -> Dir
    end.

%% -------           
%% @private
create_watch_map(_Pid, [], _Port, Map) -> Map;
create_watch_map(Pid, [{Path, Term} | Rest], Port, Map) ->
    NewMap = case filelib:is_dir(Path) of
                 true -> 
		     cast(Pid,Term,Path,"."),
                     add_to_watch_map(Path, any, Term, Port, Map);
                 false -> 
                     Dir = filename:dirname(Path),
                     case filelib:is_dir(Dir) of
                         true ->
			     File = filename:basename(Path),
			     cast(Pid,Term,Dir,File),
                             add_to_watch_map(Dir, File, Term, Port, Map);
                         false ->
			     cast(Pid,Term,Dir,"."),
                             error_logger:error_msg("~p: Path ~p is not a directory",
                                                    [?DRIVER_NAME, Dir]),
                             Map
                     end end,	
    create_watch_map(Pid, Rest, Port, NewMap).

%% @private
add_to_watch_map(Dir, File, Term, Port, Map) ->
    {WatchDescriptor, NewMap} =
        case maps:find(Dir, Map) of
            error -> 
                {ok, WD} = add_watch(Dir, Port),
                {WD, maps:put(Dir, WD, Map)};
            {ok, WD} -> 
                {WD, Map}
        end,
    maps:put({WatchDescriptor, File}, {Term,Dir}, NewMap).

%% @private
add_watch(Dir, Port) ->
    erlang:port_call(Port, ?CALL_ARG, Dir).

%% --------
%% @private
%% --------
-spec watch(#state{}) -> ok.
%% --------
watch(S = #state{port = Port}) ->
    receive
        {Port, Msgs} ->
            handle_events(S, Msgs),
            watch(S);
        terminate ->
            ok;
        Msg ->
            error_logger:error_msg("~p: Unexpected message received by watcher process: ~p",
                                   [?DRIVER_NAME, Msg]),
            ok
    end.

%% @private
handle_events(_S, []) -> ok;
handle_events(S = #state{}, [Msg | Msgs]) ->
    handle_event(S, Msg),
    handle_events(S, Msgs).

%% @private
handle_event(#state{pid = Pid, watch_map = Map}, {Wd, Name}) ->
    case maps:get({Wd, any}, Map, undefined) of
        undefined -> 
            case maps:get({Wd, Name}, Map, undefined) of
                undefined -> ok;
                Term -> cast(Pid, Term, Wd, Name)
            end;
        {Term,Dir} -> cast(Pid, Term, Dir, Name)
    end.

%% @private
cast(_, Term, _, _) when is_function(Term,0) -> Term();
cast(_, Term, _, Filename) when is_function(Term,1) -> Term(Filename);
cast(_, Term, Dir, Filename) when is_function(Term,2) -> Term(Dir,Filename);
cast(Pid, Term, _, _) when is_pid(Pid) -> 
    Pid ! {?DRIVER_NAME, self(), Term}.
